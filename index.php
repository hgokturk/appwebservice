<?php
//$_SERVER['REQUEST_METHOD'] == 'POST'
// !empty($_POST)include_once 'dbClass.php';

/*
 * token oluşturma 
 */
/*$token = sha1(uniqid(mt_rand(), true));

    $bytes = openssl_random_pseudo_bytes(16, $cstrong);
    $hex   = bin2hex($bytes);

    var_dump($hex);
    //var_dump($cstrong);
    
    //echo PHP_EOL;


echo $token;exit();*/

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include_once 'dbClass.php';
    $db = new Model();
    $action = $_POST['action'];
    switch ($action){
        case "login":
            $eMail = $_POST['eMail'];
            $password = $_POST['password'];
            $db->query("SELECT id FROM kullanicilar WHERE isim=:isim AND sifre=:sifre");
            $db->bind(':isim', $eMail);
            $db->bind(':sifre', $password);

            if ($row = $db->single()) {
                $metin = array("error" => false, "message" => null, "isloggedin" => true, "id" => $row['id']);
                $json = json_encode($metin);
                echo $json;
            } else {
                $metin = array("error" => true, "message" => "Kullanıcı adı ya da şifre yanlış!", "isloggedin" => false, "id" => null);
				$json = json_encode($metin);
                echo $json;
            }
            break;
		case "mailKontrol":
            $eMail = $_POST['eMail'];

            $db->query("call spKullanici_Mail(:paramMail)");
            $db->bind(':paramMail', $eMail);

            if ($row = $db->single()) {
                $metin = array("error" => false, "message" => $row["cevapAciklama"], "isloggedin" => $row["cevap"]>0?true:false, "id" => $row["cevap"]);
                $json = json_encode($metin);
                echo $json;
            }
            break;
			
		case "yeniAnketlerKategoriId":
            
			$kategoriId = $_POST['kategoriId'];

			$db->query("call spAnketlerYeni_kategoriId(:kategoriId)");
			$db->bind(':kategoriId', $kategoriId);
            
            if ($row = $db->resultset()) {
                $metin = array("YeniAnketlerKategoriId" => $row);
				$json = json_encode($metin);
				echo $json;
            }
            break;
			

			
		case "oneCikanAnketlerKategoriId":
            $kategoriId = $_POST['kategoriId'];

            $db->query("call spAnketlerOneCikanlar_kategoriId(:kategoriId)");
            $db->bind(':kategoriId', $kategoriId);

            if ($row = $db->resultset()) {
                $metin = array("OneCikanAnketlerKategoriId" => $row);
                $json = json_encode($metin);
                echo $json;
            }
            break;
			
			
		case "seceneklerAnketId":
            $anketId = $_POST['anketId'];

            $db->query("call spSecenekler_anketId(:anketId)");
            $db->bind(':anketId', $anketId);

            if ($row = $db->resultset()) {
                $metin = array("anketSecenekleriAnketId" => $row);
                $json = json_encode($metin);
                echo $json;
            }
            break;
			
		case "oneCikanAnketler":

			$db->query("call spAnketlerOneCikanlar()");
            
            if ($row = $db->resultset()) {
                $metin = array("OneCikanAnketler" => $row);
				$json = json_encode($metin);
				echo $json;
            }
            break;
		case "kullaniciEkle":
            $paramAdSoyad = $_POST['adSoyad'];
            $paramGoogleId = $_POST['googleId'];
            $paramMail = $_POST['eMail'];
            $paramToken = $_POST['token'];

            $db->query("call spKullaniciEkle(:paramAdSoyad,:paramGoogleId,:paramMail,:paramToken)");
            $db->bind(':paramAdSoyad', $paramAdSoyad);
			$db->bind(':paramGoogleId', $paramGoogleId);
			$db->bind(':paramMail', $paramMail);
			$db->bind(':paramToken', $paramToken);

            if ($row = $db->single()) {
                $metin = array("error" => false, "message" => $row["cevapAciklama"], "isloggedin" => $row["cevap"]>0?true:false, "id" => $row["cevap"]);
                $json = json_encode($metin);
                echo $json;
            }
            break;
			
		case "kategoriGetir":
            $db->query("call spKategoriler()");

            if ($row = $db->resultset()) {//test
                $metin = array("kategoriler" => $row);
                $json = json_encode($metin);
                echo $json;
            }
            break;
        default:
            break;
    }
}