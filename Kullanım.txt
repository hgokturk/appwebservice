﻿action : yeniAnketlerKategoriId
Parametre : kategoriId

Sonuç
{
    "YeniAnketlerKategoriId": [
        {
            "id": "2",
            "soru": "En sevdiğiniz renk nedir?",
            "oySayisi": "1"
        },
        {
            "id": "1",
            "soru": "En sevdiğiniz araba markası nedir?",
            "oySayisi": "3"
        }
    ]
}

---------------------------------------------------------------
action : oneCikanAnketlerKategoriId
Parametre : kategoriId

Sonuç
{
    "OneCikanAnketlerKategoriId": [
        {
            "id": "1",
            "soru": "En sevdiğiniz araba markası nedir?",
            "oySayisi": "3"
        },
        {
            "id": "2",
            "soru": "En sevdiğiniz renk nedir?",
            "oySayisi": "1"
        }
    ]
}


---------------------------------------------------------------
action : seceneklerAnketId
Parametre : anketId

Sonuç
{
    "anketSecenekleriAnketId": [
        {
            "id": "4",
            "secenek": "Kırmızı",
            "oySayisi": "0"
        },
        {
            "id": "5",
            "secenek": "Mavi",
            "oySayisi": "0"
        },
        {
            "id": "6",
            "secenek": "Yeşil",
            "oySayisi": "0"
        },
        {
            "id": "7",
            "secenek": "Mor",
            "oySayisi": "0"
        }
    ]
}